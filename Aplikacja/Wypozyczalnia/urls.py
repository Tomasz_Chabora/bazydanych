from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^wypo/', include('wypo.urls')),
    url(r'^admin/', admin.site.urls),
]