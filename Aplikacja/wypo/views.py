from django.http import HttpResponse, Http404
from django.template import loader
from django.db.models import Avg, Count, F
from django.db import connection
from django.utils import timezone
from django.db import transaction
import time,datetime

from .models import *

def index(request):
    template = loader.get_template('wypo/index.html')
    context = {
        'uzytkownik': request.session['user'] if 'user' in request.session else None,
        'rodzaj': request.session['type'] if 'type' in request.session else None,
        'rodzaj': request.session['type'] if 'type' in request.session else None,
    }
    return HttpResponse(template.render(context, request))

def login(request):
    #SELECT * FROM konto WHERE login=request.GET.get('login') AND haslo=request.GET.get('pass');
    attempt=Konto.objects.filter(login=request.GET.get('login'), haslo=request.GET.get('pass'))
    if attempt:
        request.session.set_expiry(0)
        request.session['user']=request.GET.get('login')
        #SELECT rodzaj FROM konto WHERE login=request.GET.get('login');
        request.session['type']=Konto.objects.filter(login=request.GET.get('login'))[0].rodzaj
        
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO historia(co,jak)\
            VALUES('{}','{}')".format("Zalogowano",request.session['user']))
        
        if request.session['type']=="Admin":
          request.session['_auth_user_id'] = 1
          request.session['_auth_user_backend'] = "django.contrib.auth.backends.ModelBackend"
          request.session['_auth_user_hash'] = "984edfb6d302633d6938e24dd49ca00e2f14c798"
    
    template = loader.get_template('wypo/login.html')
    context = {
        'login': attempt,
        'failed': (True if request.GET.get('login') or request.GET.get('pass') else None)
    }
    return HttpResponse(template.render(context, request))

def logout(request):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO historia(co,jak)\
        VALUES('{}','{}')".format("Wylogowano",request.session['user']))
    
    request.session['user']=None
    request.session['_auth_user_id'] = None
    request.session['_auth_user_backend'] = None
    request.session['_auth_user_hash'] = None
    
    template = loader.get_template('wypo/logout.html')
    context = {}
    return HttpResponse(template.render(context, request))
    
def films(request):
    search=""
    if request.GET.get('szukaj'):
        search=request.GET.get('szukaj')
    
    template = loader.get_template('wypo/filmy.html')
    context = {
        #SELECT * FROM film WHERE tytul LIKE "%search%" ORDER BY tytul;
        'film_list': Film.objects.filter(tytul__contains=search).order_by('-tytul'),
    }
    return HttpResponse(template.render(context, request))
    
def film(request, film_id):
    try:
        Film.objects.get(id=film_id)
    except Film.DoesNotExist:
        raise Http404("Podany film nie istnieje.")
    
    user=None
    ocena=None
    if 'user' in request.session:
        user=request.session['user']
    
        if request.GET.get('ocena'):
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO ocena(uzytkownik,film,ile)\
                VALUES('{}',{},{})".format(user,film_id,request.GET.get('ocena')))
        
        ocena=Ocena.objects.filter(film=film_id, uzytkownik=user)
        
    #SELECT AVG(ile) FROM ocena WHERE film=film_id;
    srednia = Ocena.objects.filter(film=film_id).aggregate(Avg('ile'))["ile__avg"]
    if srednia:
        srednia = round(srednia, 2)
    
    template = loader.get_template('wypo/film.html')
    context = {
        #SELECT * FROM film WHERE id=film_id;
        'film': Film.objects.filter(id=film_id)[0],
        'srednia': srednia,
        #SELECT COUNT(*) FROM ocena WHERE film=film_id;
        'oceny': Ocena.objects.filter(film=film_id).aggregate(Count('ile'))["ile__count"],
        'user': user,
        'ocena': ocena,
    }
    return HttpResponse(template.render(context, request))
    
def wypozyczenia(request, _konto):
    _konto=_konto[:-1]
    try:
        Konto.objects.get(login=_konto)
    except Konto.DoesNotExist:
        raise Http404("Podany użytkownik nie istnieje.")
        
    #SELECT id FROM klient WHERE konto=_konto;
    _klient = Klient.objects.filter(konto=_konto)[0].id
    
    template = loader.get_template('wypo/wypozyczenia.html')
    context = {
        'wypozyczenia':  Wypozyczenie.objects.raw('SELECT\
        wypozyczenie.id,wypozyczenie.kiedy,tytul,film.id AS id_film, ocena.ile,\
        round(julianday("now") - julianday(zaleglosc.kiedy),2) AS dni\
        FROM wypozyczenie JOIN nosnik ON nosnik.id=wypozyczenie.nosnik\
        JOIN film ON film.id=nosnik.film\
        LEFT JOIN ocena ON ocena.film=film.id AND ocena.uzytkownik="{}"\
        LEFT JOIN zaleglosc ON zaleglosc.id=wypozyczenie.id\
        WHERE wypozyczenie.klient={} ORDER BY wypozyczenie.kiedy DESC'.format(_konto,_klient)),
    }
    return HttpResponse(template.render(context, request))

def rent(request):
    klient=None
    nosnik=None
    
    if request.GET.get('klient') and request.GET.get('nosnik'):
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO wypozyczenie(klient,nosnik,kiedy)\
            VALUES('{}',{},'{}')".format(request.GET.get('klient'),request.GET.get('nosnik'),timezone.now().date()))
                
    template = loader.get_template('wypo/rent.html')
    context = {
        'klienci': Klient.objects.all(),
        'nosniki': Nosnik.objects.raw("SELECT nosnik.id FROM nosnik\
        JOIN film ON nosnik.film=film.id\
        EXCEPT\
        SELECT nosnik FROM wypozyczenie WHERE NOT oddane\
        ORDER BY nosnik.id"),
    }
    
    return HttpResponse(template.render(context, request))

def debts(request):
    #SELECT * FROM zaleglosc JOIN klient ON klient.id=zaleglosc.klient JOIN nosnik ON nosnik.id=zaleglosc.nosnik JOIN film ON film.id=nosnik.film;
    zal=Zaleglosc.objects.all().select_related().select_related()
    
    for z in zal:
        z.days=(timezone.now().date() - z.kiedy)
        z.debt=round((z.days.days/30)**2 + z.days.days/10, 2)
  
    template = loader.get_template('wypo/debts.html')
    context = {
        'zaleglosci': zal,
    }
    return HttpResponse(template.render(context, request))

def klient(request,klient):
    try:
        Klient.objects.get(id=klient)
    except Klient.DoesNotExist:
        raise Http404("Podany klient nie istnieje.")
        
    template = loader.get_template('wypo/klient.html')
    context = {
        #SELECT * FROM zaleglosc LEFT JOIN adres ON adres.id=klient.adres LEFT JOIN konto ON konto.login=klient.konto WHERE klient.id=klient;
        'klient': Klient.objects.filter(id=klient).select_related()[0],
    }
    return HttpResponse(template.render(context, request))

def zwrot(request):
    search=""
    if request.GET.get('szukaj'):
        search=request.GET.get('szukaj')
    
    wypos=Wypozyczenie.objects.raw("SELECT wypozyczenie.id FROM wypozyczenie\
            JOIN nosnik ON nosnik.id=wypozyczenie.nosnik\
            JOIN klient ON klient.id=wypozyczenie.klient\
            JOIN film ON film.id=nosnik.film\
            WHERE NOT wypozyczenie.oddane AND klient.imie||' '||klient.nazwisko LIKE '%{}%'\
            ORDER BY wypozyczenie.kiedy".format(search))
    
    if request.GET.get('zwrot'):
        with connection.cursor() as cursor:
            cursor.execute("UPDATE wypozyczenie SET oddane=1 WHERE id={}".format(request.GET.get('zwrot')))
    
    l = []
    #SELECT * FROM zaleglosc;
    for zal in Zaleglosc.objects.all():
        l.append(zal.id)
        
    template = loader.get_template('wypo/zwrot.html')
    context = {
        'wypozyczenia': wypos,
        'zaleglosci': l,
    }
    return HttpResponse(template.render(context, request))

def aktywnosc(request):
    #SELECT * FROM historia;
    historia=Historia.objects.all()
    
    if request.GET.get('od') and request.GET.get('do'):
        historia=Historia.objects.raw("SELECT * FROM historia WHERE kiedy BETWEEN '{}' AND '{}'".format(\
            request.GET.get('od'), request.GET.get('do')))
    
    template = loader.get_template('wypo/aktywnosc.html')
    context = {
        'historia': historia,
        'od': request.GET.get('od'),
        'do': request.GET.get('do'),
    }
    return HttpResponse(template.render(context, request))

def newaccount(request):
    result=None
    if request.GET.get('login') and request.GET.get('klient'):
        if (not request.GET.get('password')) or request.GET.get('password') != request.GET.get('password2') or\
        (not request.GET.get('e-mail')):
            result="Fail"
        else:
            result="OK"
            set=Konto.objects.raw("SELECT * FROM konto WHERE login='{}' OR email='{}'".\
                format(request.GET.get('login'),request.GET.get('e-mail')))
            for k in set:
                result="Fail"
    elif request.GET.get('password') or request.GET.get('password2') or request.GET.get('e-mail'):
        result="Fail"
    
    if result=="OK":
        #START TRANSACTION;
        with transaction.atomic():
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO konto(login,haslo,email,rodzaj)\
                    VALUES('{}','{}','{}','Klient')".format(request.GET.get('login'),\
                    request.GET.get('password'),request.GET.get('e-mail')))
                
                cursor.execute("UPDATE klient SET konto='{}' WHERE id={}".format(request.GET.get('login'),request.GET.get('klient')))
        #COMMIT;
    
    template = loader.get_template('wypo/newaccount.html')
    context = {
        #SELECT * FROM klient WHERE konto IS NULL;
        'klienci': Klient.objects.filter(konto=None),
        'result': result,
    }
    return HttpResponse(template.render(context, request))

def changepassword(request):
    if not 'user' in request.session:
        raise Http403("Niezalogowany.")
    
    result=None
    if request.GET.get('passwold') or request.GET.get('password') or request.GET.get('password2'):
        #SELECT haslo FROM konto WHERE login=request.session["user"];
        if Konto.objects.filter(login=request.session["user"]).first().haslo==request.GET.get('passwold') and\
        request.GET.get('password')==request.GET.get('password2'):
            result="OK"
            with connection.cursor() as cursor:
                cursor.execute("UPDATE konto SET haslo='{}' WHERE login='{}'"\
                    .format(request.GET.get('password'),request.session["user"]))
        else:
            result="Fail"
    
    if request.GET.get('usun1') and request.GET.get('usun2'):
        #START TRANSACTION;
        with transaction.atomic():
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM konto WHERE login='{}'".format(request.session["user"]))
                cursor.execute("DELETE FROM ocena WHERE uzytkownik='{}'".format(request.session["user"]))
                cursor.execute("UPDATE klient SET konto=NULL WHERE konto='{}'".format(request.session["user"]))
        #COMMIT;
        request.session['user']=None
        request.session['_auth_user_id'] = None
        request.session['_auth_user_backend'] = None
        request.session['_auth_user_hash'] = None
        result="SELF-RIP"
    
    if request.GET.get('ripklient'):
        #SELECT * FROM klient WHERE id=request.GET.get('ripklient');
        klient=Klient.objects.filter(id=request.GET.get('ripklient')).first()
        #START TRANSACTION;
        with transaction.atomic():
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM konto WHERE login='{}'".format(klient.konto))
                cursor.execute("DELETE FROM ocena WHERE uzytkownik='{}'".format(klient.konto))
                cursor.execute("DELETE FROM klient WHERE id='{}'".format(klient.id))
                cursor.execute("DELETE FROM adres WHERE id='{}'".format(klient.adres))
                cursor.execute("DELETE FROM wypozyczenie WHERE klient='{}'".format(klient.id))
        #COMMIT;
        result="RIP"
    
    template = loader.get_template('wypo/changepassword.html')
    context = {
        #SELECT * FROM konto WHERE login=request.session["user"];
        'konto': Konto.objects.filter(login=request.session["user"]).first(),
        'result': result,
        #SELECT * FROM klient;
        'klienci': Klient.objects.all(),
    }
    return HttpResponse(template.render(context, request))

def dodaj(request):
    result = None
    if all(request.GET.get(k) for k in ("imie","nazwisko","miejscowosc","dom","mieszkanie","poczta")):
        result = "OK"
        #START TRANSACTION;
        with transaction.atomic():
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO adres(miasto,ulica,numer_domu,numer_mieszkania,kod_pocztowy)\
                VALUES('{}','{}',{},{},'{}')".format(request.GET.get("miejscowosc"),request.GET.get("ulica"),request.GET.get("dom"),request.GET.get("mieszkanie"),request.GET.get("poczta")))
                
                lastAddres=Adres.objects.all().last().id
                telefon="NULL"
                if request.GET.get("telefon") and len(request.GET.get("telefon"))==9:
                    telefon=request.GET.get("telefon")
                
                cursor.execute("INSERT INTO klient(imie,nazwisko,adres,telefon)\
                VALUES('{}','{}',{},'{}')".format(request.GET.get("imie"),request.GET.get("nazwisko"),lastAddres,telefon))
        #COMMIT;
    elif any(request.GET.get(k) for k in ("imie","nazwisko","miejscowosc","dom","mieszkanie","poczta")):
        result = "Fail"
    
    template = loader.get_template('wypo/dodaj.html')
    context = {
        'result': result
    }
    return HttpResponse(template.render(context, request))