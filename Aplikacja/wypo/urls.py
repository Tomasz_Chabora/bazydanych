from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    # ex: /wypo/filmy/5
    url(r'^filmy/(?P<film_id>[0-9]+)', views.film, name='film'),
    # ex: /wypo/wypozyczenia/anon
    url(r'^wypozyczenia/(?P<_konto>.+)', views.wypozyczenia, name='wypozyczenia'),
    # ex: /wypo/klient/3
    url(r'^klient/(?P<klient>[0-9]+)', views.klient, name='klient'),
    url(r'^filmy', views.films, name='films'),
    # ex: /wypo/login/?login=anon&pass=jegohaslo
    url(r'^login', views.login, name='login'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^wypozycz', views.rent, name='rent'),
    url(r'^dluznicy', views.debts, name='debts'),
    url(r'^oddaj', views.zwrot, name='zwrot'),
    url(r'^aktywnosc', views.aktywnosc, name='aktywnosc'),
    url(r'^changepassword', views.changepassword, name='changepassword'),
    url(r'^signup', views.newaccount, name='newaccount'),
    url(r'^dodaj', views.dodaj, name='dodaj'),
]