# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Adres(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    miasto = models.CharField(max_length=64)
    ulica = models.CharField(max_length=64)
    numer_domu = models.IntegerField()
    numer_mieszkania = models.IntegerField(blank=True, null=True)
    kod_pocztowy = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'adres'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Film(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    tytul = models.CharField(max_length=255)
    rok = models.SmallIntegerField()
    rezyser = models.CharField(max_length=64)
    gatunek = models.TextField(blank=True, null=True)  # This field type is a guess.
    opis = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'film'
        unique_together = (('tytul', 'rok', 'rezyser'),)


class Klient(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    imie = models.CharField(max_length=32)
    nazwisko = models.CharField(max_length=32)
    adres = models.ForeignKey(
      "Adres", null=False,
      db_column="adres", 
      related_name="adres"
    )
    telefon = models.IntegerField(blank=True, null=True)
    konto = models.ForeignKey(
      "Konto", null=True,
      db_column="konto", 
      related_name="konto"
    )

    class Meta:
        managed = True
        db_table = 'klient'


class Konto(models.Model):
    login = models.CharField(primary_key=True, max_length=32)
    haslo = models.CharField(max_length=32)
    email = models.CharField(max_length=32)
    rodzaj = models.TextField()  # This field type is a guess.

    class Meta:
        managed = True
        db_table = 'konto'


class Nosnik(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    film = models.ForeignKey(
      "Film", null=False,
      db_column="film", 
      related_name="film"
    )
    rodzaj = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'nosnik'


class Ocena(models.Model):
    id = models.IntegerField(primary_key=True)
    film = models.IntegerField()
    uzytkownik = models.CharField(max_length=32)
    ile = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'ocena'
        unique_together = (('film', 'uzytkownik'),)


class Pracownik(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    imie = models.CharField(max_length=32)
    nazwisko = models.CharField(max_length=32)
    adres = models.IntegerField()
    telefon = models.IntegerField(blank=True, null=True)
    stanowisko = models.CharField(max_length=32)
    konto = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pracownik'


class Wypozyczenie(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nosnik = models.ForeignKey(
      "Nosnik", null=False,
      db_column="nosnik", 
      related_name="nosnik"
    )
    kiedy = models.DateField()
    klient = models.ForeignKey(
      "Klient", null=False,
      db_column="klient",
      related_name="klients"
    )
    oddane = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'wypozyczenie'


class Zaleglosc(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nosnik = models.ForeignKey(
      "Nosnik", null=False,
      db_column="nosnik", 
      related_name="nosnikz"
    )
    kiedy = models.DateField()
    klient = models.ForeignKey(
      "Klient", null=False,
      db_column="klient",
      related_name="klienty"
    )
    oddane = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'zaleglosc'


class DostepnyNosnik(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    film = models.ForeignKey(
      "Film", null=False,
      db_column="film", 
      related_name="dfilm"
    )
    rodzaj = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'dostepny_nosnik'


class Historia(models.Model):
    kiedy = models.TextField(primary_key=True)  # This field type is a guess.
    co = models.CharField(max_length=255)
    jak = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'historia'