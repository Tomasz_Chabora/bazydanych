from django.contrib import admin

from .models import *

admin.site.register(Klient)
admin.site.register(Adres)
admin.site.register(Konto)
admin.site.register(Film)
admin.site.register(Ocena)
admin.site.register(Wypozyczenie)
admin.site.register(Nosnik)
admin.site.register(Pracownik)