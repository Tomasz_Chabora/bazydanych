from django.apps import AppConfig


class WypoConfig(AppConfig):
    name = 'wypo'
