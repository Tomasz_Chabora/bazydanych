-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: CHABORA
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adres`
--

DROP TABLE IF EXISTS `adres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miasto` varchar(64) COLLATE latin2_bin NOT NULL,
  `ulica` varchar(64) COLLATE latin2_bin NOT NULL,
  `numer_domu` int(4) NOT NULL,
  `numer_mieszkania` int(4) DEFAULT NULL,
  `kod_pocztowy` varchar(64) COLLATE latin2_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adres`
--

LOCK TABLES `adres` WRITE;
/*!40000 ALTER TABLE `adres` DISABLE KEYS */;
INSERT INTO `adres` VALUES (1,'Kraków','Ulicowa',32,NULL,'22-505'),(2,'Kraków','Alejowa',47,NULL,'22-506'),(3,'Kraków','Kobierzyńska',13,14,'76-330'),(4,'Kraków','Grota Roweckiego',25,92,'55-660'),(5,'Kraków','Kobierzyńska',80,13,'76-330'),(6,'Kraków','Tomasza Janiszewskiego',72,37,'8-456'),(7,'Kraków','Daniela Chodowieckiego',86,46,'1-690'),(8,'Słomniki','Walerego Sławka',94,35,'9-939'),(9,'Krzeszowice','Iwonicka',99,44,'76-371'),(10,'Świątniki Górne','Trzech Króli',15,11,'45-332'),(11,'Świątniki Górne','Miejscowa',37,3,'30-156'),(12,'Świątniki Górne','Danusi Jurandówny',27,44,'35-104'),(13,'Świątniki Górne','Żywiecka',30,32,'72-283'),(14,'Kraków','ks. Wincentego Turka',76,43,'34-983'),(15,'Skała','Zygmunta Krasińskiego',87,7,'81-938'),(16,'Kraków','Spółdzielcze',24,17,'37-356'),(17,'Świątniki Górne','Zbigniewa Skąpskiego',26,5,'58-316'),(18,'Skawina','Wysokie',45,18,'14-676'),(19,'Skawina','Orszańska',47,40,'89-835'),(20,'Skawina','',72,1,'66-899'),(21,'Słomniki','Szczecińska',64,21,'95-948'),(22,'Krzeszowice','gen. Franciszka Kleeberga',63,41,'28-5'),(23,'Krzeszowice','gen. Stanisława Rostworowskiego',63,39,'92-17'),(24,'Świątniki Górne','Światowida',66,15,'28-212'),(25,'Krzeszowice','Stanisława Balickiego',38,38,'44-151'),(26,'Krzeszowice','Pustynna',42,1,'88-729'),(27,'Słomniki','Stefana Żeromskiego',21,42,'66-734'),(28,'Słomniki','Gnieźnieńska',16,45,'54-295'),(29,'Skała','Karola Homolacsa',5,22,'50-235'),(30,'Krzeszowice','Józefa Kałuży',43,17,'83-646'),(31,'Skawina','Soboniowicka',14,35,'72-552'),(32,'Świątniki Górne','Bastionowa',79,14,'86-451'),(33,'Świątniki Górne','Ludowa',83,22,'60-424'),(34,'Skała','ks. Wincentego Turka',82,6,'28-630'),(35,'Skawina','Heleny Marusarzówny',79,41,'51-136'),(36,'Krzeszowice','Dereniowa',63,32,'92-61'),(37,'Kraków','Strumyk',69,17,'83-906'),(38,'Krzeszowice','Wadowicka',32,41,'32-642'),(39,'Krzeszowice','Batalionów Chłopskich',1,1,'79-348'),(40,'Kraków','Józefa Ostafina',94,6,'35-285'),(41,'Skawina','Józefa Ostafina',6,2,'24-346'),(42,'Kraków','Morwowa',13,25,'79-676'),(43,'Kraków','Ratajska',68,27,'18-934'),(44,'Skawina','gen. Tadeusza Kościuszki',99,14,'51-402'),(45,'Słomniki','Oświęcimska',19,1,'97-519'),(46,'Skawina','Czołgistów',52,49,'75-658'),(47,'Skała','Tatarska',15,40,'20-676'),(48,'Krzeszowice','Ignacego Mościckiego',17,20,'31-301'),(49,'Kraków','Karola Irzykowskiego',89,47,'19-905'),(50,'Skała','Podłęska',18,24,'91-827'),(51,'Świątniki Górne','Suche Łąki',31,48,'82-139'),(52,'Skawina','Jana Kilińskiego',81,25,'68-363'),(53,'Skawina','Akademicka',62,43,'91-490'),(54,'Świątniki Górne','Jana Sas-Zubrzyckiego',42,43,'9-763'),(55,'Słomniki','Most Wandy',74,47,'37-440'),(56,'Kraków','Heleny Marusarzówny',23,37,'4-781'),(57,'Kraków','Oświęcimska',46,5,'26-384'),(58,'Kraków','Tatarska',94,23,'23-53'),(59,'Kraków','Miejscowa',82,22,'85-377'),(60,'Kraków','Zduńska',55,4,'26-825');
/*!40000 ALTER TABLE `adres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `dluznik`
--

DROP TABLE IF EXISTS `dluznik`;
/*!50001 DROP VIEW IF EXISTS `dluznik`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dluznik` AS SELECT 
 1 AS `imie`,
 1 AS `nazwisko`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tytul` varchar(255) COLLATE latin2_bin NOT NULL,
  `rok` year(4) NOT NULL,
  `rezyser` varchar(64) COLLATE latin2_bin NOT NULL,
  `gatunek` enum('Sensacyjny','Horror','Komedia','Dramat') COLLATE latin2_bin DEFAULT NULL,
  `opis` text COLLATE latin2_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filmy` (`tytul`,`rok`,`rezyser`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film`
--

LOCK TABLES `film` WRITE;
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` VALUES (1,'King Kong',2005,'Peter Jackson','Sensacyjny',NULL),(2,'Kill Bill',2003,'Quentin Tarantino','Sensacyjny',NULL),(3,'Terminator',1984,'James Cameron','Sensacyjny',NULL),(4,'Władca Pierścieni: Drużyna Pierścienia',2001,'Peter Jackson','Sensacyjny',NULL),(5,'Gwiezdne Wojny: Nowa Nadzieja',1977,'George Lucas','Sensacyjny',NULL),(6,'Monty Python i Święty Graal',1975,'Terry Gilliam','Komedia',NULL),(7,'Listy do M.',2011,'Mitja Okorn','Komedia',NULL),(8,'Bruce Wszechmogący',2003,'Tom Shadyac','Komedia',NULL),(9,'Epoka Lodowcowa',2002,'Chris Wedge','Komedia',NULL),(10,'Dyktator',2012,'Larry Charles','Komedia',NULL),(11,'Lśnienie',1980,'Stanley Kubrick','Horror',NULL),(12,'The Ring: Krąg',1998,'Hideo Nakata','Horror',NULL),(13,'Koszmar z ulicy Wiązów',1984,'Wes Craven','Horror',NULL),(14,'Piątek, trzynastego',1980,'Sean S. Cunningham','Horror',NULL),(15,'Noc żywych trupów',1968,'George Romero','Horror',NULL),(16,'Skazani na Shawshank',1994,'Frank Darabont','Dramat',NULL),(17,'Nietykalni',2011,'Olivier Nakache','Dramat',NULL),(18,'Zielona mila',1999,'Frank Darabont','Dramat',NULL),(19,'Forrest Gump',1994,'Robert Zemeckis','Dramat',NULL),(20,'Lot nad kukułczym gniazdem',1975,'Miloš Forman','Dramat',NULL);
/*!40000 ALTER TABLE `film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klient`
--

DROP TABLE IF EXISTS `klient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imie` varchar(32) COLLATE latin2_bin NOT NULL,
  `nazwisko` varchar(32) COLLATE latin2_bin NOT NULL,
  `adres` int(11) NOT NULL,
  `telefon` int(9) DEFAULT NULL,
  `konto` varchar(32) COLLATE latin2_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `konta` (`konto`),
  KEY `adres` (`adres`),
  KEY `konto` (`konto`),
  CONSTRAINT `adres` FOREIGN KEY (`adres`) REFERENCES `adres` (`id`),
  CONSTRAINT `konto` FOREIGN KEY (`konto`) REFERENCES `konto` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klient`
--

LOCK TABLES `klient` WRITE;
/*!40000 ALTER TABLE `klient` DISABLE KEYS */;
INSERT INTO `klient` VALUES (1,'Jan','Kowalski',1,NULL,'drifterross'),(2,'Janusz','Nowak',2,NULL,'cashhumiliated'),(3,'Marcin','Ciarcin',3,NULL,'protostarhave'),(4,'Marek','Anon',4,NULL,'casesbuckpasser'),(5,'Barbara','Rydz',5,NULL,NULL),(6,'Joanna','Kalinowski',6,652981360,'albanyminette'),(7,'Marcin','Maćkowiak',7,712390076,'reserveddesired'),(8,'Andrzej','Dębowski',8,NULL,'snowdonsolomon'),(9,'Małgorzata','Panek',9,892755424,NULL),(10,'Ewa','Osiński',10,238880629,NULL),(11,'Paweł','Nowaczyk',11,975249281,'foxbirchwood'),(12,'Janina','Kaczyński',12,457302002,'immaterialheidi'),(13,'Elżbieta ','Zając',13,478450716,'wallcounter'),(14,'Joanna','Rak',14,NULL,'flushedlucky'),(15,'Katarzyna','Florczak',15,887528768,NULL),(16,'Zofia','Ptak',16,672742522,'areczek96'),(17,'Piotr','Łuczak',17,NULL,'cookedethanoic'),(18,'Janina','Fijałkowski',18,NULL,'ambushscalby'),(19,'Małgorzata','Stec',19,NULL,'chattingrigmarole'),(20,'Tadeusz','Morawski',20,380965073,NULL),(21,'Andrzej','Adamczyk',21,NULL,'angleloy'),(22,'Irena','Bąk',22,819868772,NULL),(23,'Janina','Adamczyk',23,NULL,'cosmicmyview'),(24,'Tadeusz','Bednarczyk',24,NULL,'unikalnynick2'),(25,'Aleksandra','Markowski',25,132491977,NULL),(26,'Zofia','Krakowiak',26,190974262,'curdoverdue'),(27,'Mateusz','Czajkowski',27,NULL,'charlievenn'),(28,'Maria','Wójtowicz',28,NULL,'setupannounces'),(29,'Teresa','Nowacki',29,325372984,NULL),(30,'Jan','Kruk',30,NULL,NULL),(31,'Ewa','Morawski',31,NULL,'uprightbafflement'),(32,'Maria','Jurek',32,NULL,'caramelizejack!'),(33,'Danuta','Adamczyk',33,NULL,'amphoralists'),(34,'Magdalena','Turek',34,NULL,'unikalnynick'),(35,'Stanisław','Bednarski',35,NULL,'chestbitter'),(36,'Irena','Przybylski',36,NULL,'filoglondacz'),(37,'Jan','Zakrzewski',37,838287336,NULL),(38,'Marek','Ciesielski',38,347624909,'bawdsnatch'),(39,'Adam','Banasiak',39,878574855,'mercedesridiculous'),(40,'Danuta','Krajewski',40,94912174,NULL),(41,'Anna','Borowski',41,165941822,'roaringfivehead'),(42,'Jan','Jędrzejczyk',42,NULL,NULL),(43,'Danuta','Zarzycki',43,16509133,NULL),(44,'Jan','Lisowski',44,NULL,'scentedrocky'),(45,'Maria','Witek',45,NULL,'snizortbrandy'),(46,'Zbigniew','Kasprzyk',46,NULL,NULL),(47,'Katarzyna','Kłos',47,NULL,NULL),(48,'Michał','Pawlik',48,92053749,'iowaltz'),(49,'Irena','Klimczak',49,NULL,'wiltedbandy'),(50,'Tomasz','Witkowski',50,862032046,NULL),(51,'Dariusz','Dziuba',51,NULL,'olliestocks'),(52,'Elżbieta ','Lech',52,NULL,NULL),(53,'Krystyna','Bieniek',53,NULL,'aerobicflax'),(54,'Łukasz','Gruszczyński',54,NULL,NULL),(55,'Mateusz','Gawroński',55,86196365,'sleetybeloved');
/*!40000 ALTER TABLE `klient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `kontakt`
--

DROP TABLE IF EXISTS `kontakt`;
/*!50001 DROP VIEW IF EXISTS `kontakt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `kontakt` AS SELECT 
 1 AS `imie`,
 1 AS `nazwisko`,
 1 AS `telefon`,
 1 AS `email`,
 1 AS `miasto`,
 1 AS `ulica`,
 1 AS `numer_domu`,
 1 AS `numer_mieszkania`,
 1 AS `kod_pocztowy`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `konto`
--

DROP TABLE IF EXISTS `konto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konto` (
  `login` varchar(32) COLLATE latin2_bin NOT NULL,
  `haslo` varchar(32) COLLATE latin2_bin NOT NULL,
  `email` varchar(32) COLLATE latin2_bin NOT NULL,
  `rodzaj` enum('Klient','Pracownik','Admin') COLLATE latin2_bin NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `konto`
--

LOCK TABLES `konto` WRITE;
/*!40000 ALTER TABLE `konto` DISABLE KEYS */;
INSERT INTO `konto` VALUES ('aerobicflax','k6;^Z=Oe0Xb','aer.obicflax@o2.pl','Klient'),('albanyminette','$x<7x','albany.minette@yahoo.com','Klient'),('ambushscalby','Z7mG<','ambu.shscalby@o2.pl','Klient'),('amphoralists','oG(v_kqG^8GF','a.mphoralists@o2.pl','Klient'),('angleloy','G5CG0?eR8','ang.leloy@gmail.com','Klient'),('areczek96','$#nve','arkos@o2.pl','Klient'),('bawdsnatch','nI>H\'A#x','bawd.snatch@yahoo.com','Klient'),('caramelizejack!','mtYCH?GqsC9:','cara.melizejack!@o2.pl','Klient'),('casesbuckpasser','Dr@D','casesb.uckpasser@yahoo.com','Klient'),('cashhumiliated','$d90','c.ashhumiliated@yahoo.com','Klient'),('charlievenn','vkj][0OQ]','ch.arlievenn@gmail.com','Klient'),('chattingrigmarole','dVXwuJfE_/op/','chatting.rigmarole@yahoo.com','Klient'),('chestbitter','eyaef','chestb.itter@gmail.com','Klient'),('cookedethanoic','E[xXEfm','cook.edethanoic@o2.pl','Klient'),('cosmicmyview','||Juo','co.smicmyview@wp.pl','Klient'),('curdoverdue','8L]NlVK','c.urdoverdue@o2.pl','Klient'),('drifterross','zaAyHGA>eMou','drif.terross@o2.pl','Klient'),('filoglondacz','uuuuua','mymail@yahoo.com','Klient'),('flushedlucky','VtwG','flushe.dlucky@yahoo.com','Klient'),('foxbirchwood','qH#JgqD26','foxbir.chwood@wp.pl','Klient'),('immaterialheidi','crE3Z@qUm6tm','immaterialh.eidi@yahoo.com','Klient'),('iowaltz','q{u^2','iow.altz@wp.pl','Klient'),('mercedesridiculous','R6LWye>Z1DtL','mercedesridic.ulous@o2.pl','Klient'),('olliestocks','wEM{D','o.lliestocks@o2.pl','Klient'),('protostarhave','WUPE_rJzne=','prot.ostarhave@yahoo.com','Klient'),('reserveddesired','<8=4','res.erveddesired@o2.pl','Klient'),('roaringfivehead','kuYxt5>]tnd=','roaringfi.vehead@o2.pl','Klient'),('scentedrocky','h`fJX@29?2Mz/','scen.tedrocky@gmail.com','Klient'),('setupannounces','x9jYl/s4B','s.etupannounces@gmail.com','Klient'),('sleetybeloved','kZuV3N;','sleetyb.eloved@wp.pl','Klient'),('snizortbrandy','loT4z','sni.zortbrandy@wp.pl','Klient'),('snowdonsolomon','?\'(U\'e3&^<','snowd.onsolomon@yahoo.com','Klient'),('unikalnynick','123aaa','jan.kowalski@gmail.com','Klient'),('unikalnynick2','876fs2','pan.nowak@gmail.com','Klient'),('uprightbafflement','gkRrpCfuTR[[E','uprightba.fflement@o2.pl','Klient'),('wallcounter','b2{=QlslSCS','wall.counter@o2.pl','Klient'),('wiltedbandy','JQ2Jj:|KUf<rl','wilte.dbandy@yahoo.com','Klient');
/*!40000 ALTER TABLE `konto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nosnik`
--

DROP TABLE IF EXISTS `nosnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nosnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film` int(11) NOT NULL,
  `rodzaj` enum('DVD','VHS','Blue Ray') COLLATE latin2_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nfilm` (`film`),
  CONSTRAINT `nfilm` FOREIGN KEY (`film`) REFERENCES `film` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nosnik`
--

LOCK TABLES `nosnik` WRITE;
/*!40000 ALTER TABLE `nosnik` DISABLE KEYS */;
INSERT INTO `nosnik` VALUES (1,11,'VHS'),(2,20,'Blue Ray'),(3,18,'DVD'),(4,3,'VHS'),(5,4,'Blue Ray'),(6,2,'VHS'),(7,4,'VHS'),(8,10,'VHS'),(9,4,'Blue Ray'),(10,20,'VHS'),(11,19,'Blue Ray'),(12,7,'DVD'),(13,6,'DVD'),(14,18,'VHS'),(15,9,'Blue Ray'),(16,2,'Blue Ray'),(17,11,'VHS'),(18,15,'Blue Ray'),(19,8,'VHS'),(20,10,'DVD'),(21,9,'Blue Ray'),(22,16,'VHS'),(23,17,'DVD'),(24,10,'VHS'),(25,10,'Blue Ray'),(26,18,'Blue Ray'),(27,11,'DVD'),(28,3,'DVD'),(29,16,'Blue Ray'),(30,4,'DVD'),(31,9,'Blue Ray'),(32,15,'DVD'),(33,18,'Blue Ray'),(34,12,'VHS'),(35,14,'Blue Ray'),(36,9,'Blue Ray'),(37,13,'DVD'),(38,2,'DVD'),(39,7,'DVD'),(40,16,'Blue Ray'),(41,3,'Blue Ray'),(42,13,'Blue Ray'),(43,9,'Blue Ray'),(44,2,'VHS'),(45,14,'VHS'),(46,3,'Blue Ray'),(47,12,'VHS'),(48,11,'VHS'),(49,14,'DVD'),(50,20,'Blue Ray'),(51,15,'VHS'),(52,3,'DVD'),(53,8,'DVD'),(54,17,'DVD'),(55,17,'VHS'),(56,17,'DVD'),(57,16,'DVD'),(58,2,'DVD'),(59,6,'Blue Ray'),(60,12,'VHS'),(61,16,'DVD'),(62,13,'DVD'),(63,12,'Blue Ray'),(64,12,'Blue Ray'),(65,3,'Blue Ray'),(66,1,'VHS'),(67,18,'VHS'),(68,5,'DVD'),(69,7,'DVD'),(70,7,'DVD'),(71,6,'VHS'),(72,14,'VHS'),(73,20,'DVD'),(74,14,'Blue Ray'),(75,7,'VHS'),(76,2,'VHS'),(77,16,'Blue Ray'),(78,10,'DVD'),(79,18,'VHS'),(80,19,'DVD'),(81,9,'DVD'),(82,14,'Blue Ray'),(83,8,'VHS'),(84,15,'Blue Ray'),(85,15,'VHS'),(86,1,'DVD'),(87,4,'Blue Ray'),(88,13,'Blue Ray'),(89,12,'VHS'),(90,2,'Blue Ray'),(91,11,'VHS'),(92,7,'DVD'),(93,19,'DVD'),(94,7,'DVD'),(95,7,'DVD'),(96,4,'DVD'),(97,1,'DVD'),(98,17,'VHS'),(99,3,'VHS'),(100,6,'DVD');
/*!40000 ALTER TABLE `nosnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `nosnikfilm`
--

DROP TABLE IF EXISTS `nosnikfilm`;
/*!50001 DROP VIEW IF EXISTS `nosnikfilm`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `nosnikfilm` AS SELECT 
 1 AS `id`,
 1 AS `tytul`,
 1 AS `rodzaj`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ocena`
--

DROP TABLE IF EXISTS `ocena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocena` (
  `film` int(11) NOT NULL,
  `uzytkownik` varchar(32) COLLATE latin2_bin NOT NULL,
  `ile` int(1) NOT NULL,
  UNIQUE KEY `oceny` (`film`,`uzytkownik`),
  KEY `film` (`film`),
  KEY `uzytkownikfk` (`uzytkownik`),
  CONSTRAINT `film` FOREIGN KEY (`film`) REFERENCES `film` (`id`),
  CONSTRAINT `uzytkownikfk` FOREIGN KEY (`uzytkownik`) REFERENCES `konto` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocena`
--

LOCK TABLES `ocena` WRITE;
/*!40000 ALTER TABLE `ocena` DISABLE KEYS */;
INSERT INTO `ocena` VALUES (1,'areczek96',4),(1,'curdoverdue',1),(1,'filoglondacz',5),(1,'unikalnynick',4),(1,'unikalnynick2',3),(2,'ambushscalby',2),(2,'unikalnynick',5),(2,'unikalnynick2',3),(3,'ambushscalby',3),(3,'drifterross',4),(3,'snowdonsolomon',2),(3,'unikalnynick',4),(3,'wallcounter',2),(4,'aerobicflax',1),(4,'areczek96',5),(4,'flushedlucky',4),(4,'sleetybeloved',5),(5,'albanyminette',5),(5,'ambushscalby',1),(5,'reserveddesired',5),(5,'roaringfivehead',3),(5,'uprightbafflement',3),(6,'cosmicmyview',5),(6,'sleetybeloved',1),(6,'unikalnynick',4),(6,'wallcounter',4),(7,'bawdsnatch',5),(7,'cosmicmyview',4),(7,'filoglondacz',4),(7,'protostarhave',3),(7,'uprightbafflement',1),(8,'cosmicmyview',3),(8,'setupannounces',3),(8,'unikalnynick',1),(8,'unikalnynick2',2),(8,'wiltedbandy',1),(9,'bawdsnatch',1),(9,'foxbirchwood',5),(9,'wallcounter',2),(11,'albanyminette',2),(11,'caramelizejack!',3),(11,'chestbitter',2),(11,'mercedesridiculous',4),(11,'wiltedbandy',4),(12,'aerobicflax',4),(12,'caramelizejack!',2),(12,'cashhumiliated',1),(12,'wallcounter',5),(13,'bawdsnatch',3),(13,'charlievenn',2),(13,'flushedlucky',5),(13,'immaterialheidi',5),(13,'uprightbafflement',2),(14,'curdoverdue',2),(14,'mercedesridiculous',1),(15,'chestbitter',2),(15,'mercedesridiculous',5),(15,'reserveddesired',2),(15,'sleetybeloved',1),(16,'albanyminette',5),(16,'bawdsnatch',4),(16,'caramelizejack!',3),(16,'snizortbrandy',1),(17,'angleloy',3),(17,'cashhumiliated',2),(17,'chattingrigmarole',1),(17,'cosmicmyview',4),(17,'roaringfivehead',2),(18,'cashhumiliated',1),(18,'curdoverdue',4),(18,'uprightbafflement',4),(19,'chattingrigmarole',4),(19,'filoglondacz',4),(19,'unikalnynick',5),(20,'albanyminette',2),(20,'chestbitter',2),(20,'mercedesridiculous',5),(20,'olliestocks',1),(20,'scentedrocky',2),(20,'setupannounces',1),(20,'snizortbrandy',1),(20,'unikalnynick',5),(20,'wallcounter',4);
/*!40000 ALTER TABLE `ocena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pracownik`
--

DROP TABLE IF EXISTS `pracownik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pracownik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imie` varchar(32) COLLATE latin2_bin NOT NULL,
  `nazwisko` varchar(32) COLLATE latin2_bin NOT NULL,
  `adres` int(11) NOT NULL,
  `telefon` int(9) DEFAULT NULL,
  `stanowisko` varchar(32) COLLATE latin2_bin NOT NULL,
  `konto` varchar(32) COLLATE latin2_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `konta` (`konto`),
  KEY `adresp` (`adres`),
  CONSTRAINT `adresp` FOREIGN KEY (`adres`) REFERENCES `adres` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pracownik`
--

LOCK TABLES `pracownik` WRITE;
/*!40000 ALTER TABLE `pracownik` DISABLE KEYS */;
INSERT INTO `pracownik` VALUES (1,'Andrzej','Dobrowolski',56,779937841,'Kierownik',NULL),(2,'Danuta','Chmiel',57,741533131,'Zastępca',NULL),(3,'Janina','Drzewiecki',58,5618491,'Sprzedawca',NULL),(4,'Jadwiga','Koza',59,517607293,'Sprzedawca',NULL),(5,'Małgorzata','Przybysz',60,359410524,'Sprzedawca',NULL);
/*!40000 ALTER TABLE `pracownik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wypozyczenie`
--

DROP TABLE IF EXISTS `wypozyczenie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wypozyczenie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nosnik` int(11) NOT NULL,
  `kiedy` date NOT NULL,
  `zaleglosc` int(11) DEFAULT NULL,
  `klient` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nosnik` (`nosnik`),
  KEY `zaleglosc` (`zaleglosc`),
  KEY `klient` (`klient`),
  CONSTRAINT `klient` FOREIGN KEY (`klient`) REFERENCES `klient` (`id`),
  CONSTRAINT `nosnik` FOREIGN KEY (`nosnik`) REFERENCES `nosnik` (`id`),
  CONSTRAINT `zaleglosc` FOREIGN KEY (`zaleglosc`) REFERENCES `zaleglosc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wypozyczenie`
--

LOCK TABLES `wypozyczenie` WRITE;
/*!40000 ALTER TABLE `wypozyczenie` DISABLE KEYS */;
INSERT INTO `wypozyczenie` VALUES (1,94,'2011-11-04',40,18),(2,77,'2016-11-28',NULL,30),(3,42,'2016-10-07',NULL,43),(4,49,'2014-08-17',NULL,17),(5,44,'2014-01-20',NULL,12),(6,76,'2013-12-11',NULL,11),(7,62,'2010-09-27',NULL,36),(8,3,'2016-01-18',NULL,11),(9,19,'2012-02-19',NULL,38),(10,9,'2010-05-24',NULL,49),(11,70,'2011-01-09',NULL,33),(12,9,'2016-04-13',NULL,43),(13,23,'2016-06-27',NULL,43),(14,54,'2012-04-19',NULL,46),(15,24,'2013-01-15',34,11),(16,1,'2010-05-03',23,33),(17,55,'2011-08-28',NULL,42),(18,21,'2016-09-27',NULL,35),(19,78,'2013-12-28',NULL,42),(20,72,'2015-09-25',21,47),(21,39,'2010-12-06',NULL,27),(22,85,'2012-05-01',17,13),(23,92,'2012-07-23',NULL,32),(24,45,'2010-09-07',NULL,7),(25,54,'2011-07-09',NULL,55),(26,45,'2010-07-05',NULL,33),(27,1,'2010-05-21',NULL,34),(28,31,'2013-03-09',NULL,23),(29,92,'2014-04-02',NULL,37),(30,71,'2015-10-27',NULL,29),(31,1,'2010-10-25',NULL,42),(32,26,'2010-09-21',NULL,28),(33,6,'2016-09-18',15,41),(34,99,'2011-08-28',NULL,24),(35,73,'2013-09-01',NULL,1),(36,42,'2014-10-14',NULL,32),(37,42,'2012-02-07',NULL,28),(38,82,'2010-08-09',NULL,29),(39,45,'2010-02-18',NULL,6),(40,75,'2011-08-20',NULL,46),(41,99,'2014-07-28',7,45),(42,99,'2013-10-24',NULL,31),(43,83,'2014-05-16',2,47),(44,83,'2014-03-25',NULL,3),(45,61,'2010-05-13',NULL,17),(46,37,'2011-12-01',NULL,7),(47,18,'2014-01-20',NULL,17),(48,98,'2016-05-12',12,24),(49,73,'2010-05-23',NULL,22),(50,45,'2013-03-08',28,26),(51,63,'2010-04-22',NULL,24),(52,21,'2012-04-08',NULL,45),(53,20,'2010-07-07',NULL,5),(54,97,'2015-12-14',NULL,6),(55,33,'2010-06-18',NULL,22),(56,4,'2014-07-22',18,24),(57,13,'2012-09-02',NULL,15),(58,31,'2010-01-21',NULL,31),(59,66,'2013-11-09',3,2),(60,46,'2016-08-26',NULL,14),(61,83,'2015-10-26',NULL,31),(62,35,'2016-01-12',NULL,13),(63,29,'2010-11-15',NULL,47),(64,85,'2016-07-23',26,43),(65,42,'2010-05-01',NULL,23),(66,84,'2013-10-28',NULL,14),(67,48,'2010-03-14',9,2),(68,8,'2014-12-14',NULL,25),(69,50,'2014-01-18',NULL,28),(70,74,'2014-02-02',NULL,18),(71,93,'2010-10-10',NULL,22),(72,78,'2013-05-23',NULL,50),(73,18,'2010-07-23',25,40),(74,42,'2010-08-06',NULL,28),(75,36,'2012-12-23',NULL,17),(76,17,'2012-11-23',NULL,46),(77,11,'2013-05-24',NULL,41),(78,66,'2013-01-07',NULL,9),(79,4,'2014-07-14',NULL,31),(80,85,'2015-09-21',NULL,54),(81,13,'2011-04-10',NULL,32),(82,47,'2013-12-23',NULL,34),(83,83,'2011-10-07',NULL,18),(84,44,'2016-12-14',NULL,52),(85,95,'2010-06-15',NULL,44),(86,34,'2011-03-03',NULL,18),(87,88,'2013-04-16',NULL,19),(88,93,'2015-08-02',NULL,48),(89,63,'2015-01-17',5,40),(90,25,'2011-12-07',NULL,13),(91,90,'2012-12-02',NULL,5),(92,12,'2015-01-26',NULL,23),(93,94,'2011-03-08',37,22),(94,44,'2014-11-08',NULL,12),(95,79,'2013-06-21',NULL,21),(96,65,'2016-06-04',NULL,6),(97,7,'2013-02-21',NULL,13),(98,69,'2015-07-25',NULL,25),(99,3,'2012-01-06',27,1),(100,69,'2014-11-02',NULL,24),(101,87,'2014-09-09',NULL,33),(102,85,'2014-09-08',4,20),(103,17,'2010-11-17',NULL,37),(104,32,'2016-06-11',NULL,21),(105,46,'2013-07-17',NULL,8),(106,8,'2014-09-06',NULL,8),(107,44,'2013-11-17',NULL,5),(108,94,'2010-08-04',NULL,22),(109,35,'2010-04-04',NULL,52),(110,34,'2013-04-02',NULL,8),(111,40,'2013-02-17',32,7),(112,88,'2013-01-21',35,45),(113,40,'2016-12-03',NULL,7),(114,65,'2015-02-04',14,28),(115,13,'2016-09-20',NULL,14),(116,90,'2015-02-17',31,20),(117,26,'2011-04-12',NULL,49),(118,21,'2010-05-07',NULL,20),(119,96,'2016-09-09',NULL,16),(120,88,'2013-10-13',NULL,8),(121,7,'2012-05-09',NULL,34),(122,76,'2012-08-17',38,36),(123,71,'2015-09-02',NULL,48),(124,84,'2010-09-16',NULL,22),(125,35,'2010-10-10',NULL,32),(126,11,'2015-10-27',NULL,9),(127,6,'2010-11-14',NULL,19),(128,39,'2015-11-08',11,19),(129,30,'2014-09-26',NULL,7),(130,90,'2016-11-20',NULL,5),(131,69,'2010-04-23',8,15),(132,2,'2012-08-11',NULL,46),(133,22,'2011-11-23',13,23),(134,11,'2015-05-20',NULL,22),(135,25,'2010-02-08',NULL,1),(136,16,'2015-05-04',NULL,27),(137,72,'2016-12-16',NULL,55),(138,8,'2010-02-03',NULL,15),(139,55,'2012-07-04',NULL,41),(140,58,'2016-11-09',NULL,6),(141,83,'2011-04-27',NULL,23),(142,64,'2010-07-26',NULL,54),(143,54,'2015-10-02',NULL,30),(144,93,'2012-12-03',NULL,55),(145,88,'2014-08-19',NULL,7),(146,63,'2016-06-12',NULL,34),(147,100,'2012-07-16',33,38),(148,62,'2014-05-06',20,35),(149,46,'2013-08-13',NULL,39),(150,51,'2010-02-15',39,13),(151,53,'2011-09-21',NULL,52),(152,38,'2014-04-02',NULL,34),(153,38,'2016-02-13',NULL,6),(154,67,'2012-08-18',NULL,9),(155,2,'2011-04-10',NULL,34),(156,70,'2011-11-21',NULL,2),(157,21,'2011-08-05',NULL,25),(158,43,'2015-05-25',NULL,5),(159,56,'2012-03-04',NULL,35),(160,72,'2010-12-12',NULL,32),(161,40,'2012-02-01',NULL,4),(162,87,'2012-12-03',NULL,14),(163,52,'2015-07-18',NULL,25),(164,80,'2012-03-03',NULL,5),(165,87,'2013-03-15',NULL,18),(166,49,'2013-03-05',NULL,8),(167,25,'2016-11-19',NULL,16),(168,20,'2014-08-25',22,28),(169,55,'2010-11-24',NULL,35),(170,23,'2016-09-25',19,35),(171,13,'2014-06-12',1,28),(172,95,'2011-09-02',NULL,40),(173,19,'2010-11-15',NULL,4),(174,65,'2014-10-18',NULL,52),(175,89,'2015-03-06',NULL,31),(176,64,'2016-08-03',NULL,42),(177,91,'2010-10-07',30,22),(178,51,'2011-01-24',NULL,12),(179,6,'2015-08-11',NULL,42),(180,85,'2015-02-10',NULL,47),(181,67,'2011-04-27',16,25),(182,1,'2012-02-10',NULL,44),(183,31,'2013-05-01',24,42),(184,98,'2011-07-09',NULL,32),(185,7,'2014-11-08',NULL,45),(186,81,'2016-08-08',NULL,43),(187,31,'2015-02-20',6,36),(188,19,'2015-03-04',NULL,22),(189,93,'2016-09-11',NULL,24),(190,98,'2011-11-18',NULL,1),(191,34,'2011-08-19',NULL,31),(192,90,'2015-10-09',NULL,43),(193,60,'2015-09-27',NULL,42),(194,83,'2010-03-20',NULL,33),(195,35,'2011-06-17',29,43),(196,39,'2011-01-05',NULL,54),(197,43,'2014-06-18',36,54),(198,3,'2016-03-25',10,37),(199,52,'2014-10-03',NULL,6),(200,77,'2015-08-14',NULL,55);
/*!40000 ALTER TABLE `wypozyczenie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zaleglosc`
--

DROP TABLE IF EXISTS `zaleglosc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zaleglosc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` int(11) NOT NULL,
  `wysokosc` int(11) NOT NULL,
  `splacona` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin2 COLLATE=latin2_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zaleglosc`
--

LOCK TABLES `zaleglosc` WRITE;
/*!40000 ALTER TABLE `zaleglosc` DISABLE KEYS */;
INSERT INTO `zaleglosc` VALUES (1,28,140,0),(2,40,200,1),(3,45,225,1),(4,27,135,1),(5,22,110,0),(6,25,125,1),(7,28,140,1),(8,17,85,1),(9,15,75,1),(10,35,175,1),(11,45,225,0),(12,28,140,0),(13,10,50,0),(14,24,120,1),(15,18,90,0),(16,22,110,1),(17,37,185,0),(18,7,35,1),(19,28,140,0),(20,1,5,1),(21,43,215,1),(22,46,230,1),(23,43,215,1),(24,34,170,0),(25,10,50,1),(26,37,185,0),(27,12,60,1),(28,25,125,1),(29,9,45,1),(30,36,180,1),(31,13,65,0),(32,39,195,1),(33,33,165,0),(34,50,250,1),(35,46,230,1),(36,25,125,1),(37,14,70,1),(38,12,60,0),(39,23,115,1),(40,35,175,0);
/*!40000 ALTER TABLE `zaleglosc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `dluznik`
--

/*!50001 DROP VIEW IF EXISTS `dluznik`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp852 */;
/*!50001 SET character_set_results     = cp852 */;
/*!50001 SET collation_connection      = cp852_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `dluznik` AS select `klient`.`imie` AS `imie`,`klient`.`nazwisko` AS `nazwisko` from ((`klient` join `wypozyczenie` on((`wypozyczenie`.`klient` = `klient`.`id`))) join `zaleglosc` on((`zaleglosc`.`id` = `wypozyczenie`.`zaleglosc`))) where (`zaleglosc`.`splacona` = FALSE) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `kontakt`
--

/*!50001 DROP VIEW IF EXISTS `kontakt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp852 */;
/*!50001 SET character_set_results     = cp852 */;
/*!50001 SET collation_connection      = cp852_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `kontakt` AS select `klient`.`imie` AS `imie`,`klient`.`nazwisko` AS `nazwisko`,`klient`.`telefon` AS `telefon`,`konto`.`email` AS `email`,`adres`.`miasto` AS `miasto`,`adres`.`ulica` AS `ulica`,`adres`.`numer_domu` AS `numer_domu`,`adres`.`numer_mieszkania` AS `numer_mieszkania`,`adres`.`kod_pocztowy` AS `kod_pocztowy` from ((`klient` left join `konto` on((`konto`.`login` = `klient`.`konto`))) left join `adres` on((`adres`.`id` = `klient`.`adres`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `nosnikfilm`
--

/*!50001 DROP VIEW IF EXISTS `nosnikfilm`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp852 */;
/*!50001 SET character_set_results     = cp852 */;
/*!50001 SET collation_connection      = cp852_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `nosnikfilm` AS select `nosnik`.`id` AS `id`,`film`.`tytul` AS `tytul`,`nosnik`.`rodzaj` AS `rodzaj` from (`nosnik` join `film` on((`film`.`id` = `nosnik`.`film`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-31 18:46:44
